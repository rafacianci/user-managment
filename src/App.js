import React from 'react';
import 'bulma/css/bulma.css';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import {
  BrowserRouter as Router,
  NavLink,
  Route,
} from 'react-router-dom';
import reducers from './reducers';
import Users from './containers/Users';
import UserForm from './containers/UserForm';
import Groups from './containers/Groups';
import GroupForm from './containers/GroupForm';
import './App.css';

const store = createStore(
  reducers,
  {},
  applyMiddleware(thunk),
);

const App = () => (
  <Router>
    <Provider store={store}>
      <div className='App'>
        <header>
          <nav className='App-header container'>
            <h1>Test</h1>
            <ul>
              <li>
                <NavLink exact activeClassName='active' to='/'>
                  Users
                </NavLink>
              </li>
              <li>
                <NavLink exact activeClassName='active' to='/groups'>
                  Group
                </NavLink>
              </li>
            </ul>
          </nav>
        </header>
        <Route exact path='/' component={Users} />
        <Route exact path='/groups' component={Groups} />
        <Route path='/user/:userId' component={UserForm} />
        <Route path='/group/:groupId' component={GroupForm} />
      </div>
    </Provider>
  </Router>
);

export default App;
