import {
  GET_USER,
  CLEAR_USER_EDITION,
  SAVE_USER,
  DELETE_USER,
} from '../actions/types';

const initialState = {
  data: [
    {
      name: 'Goku',
      id: 1,
    },
    {
      name: 'Vegeta',
      id: 2,
    },
    {
      name: 'Gohan',
      id: 3,
    },
  ],
};

export default (state = initialState, action) => {
  if (action.type === GET_USER) {
    return {
      ...state,
      user: state.data.find((user) => user.id === parseInt(action.payload, 0)),
    };
  }

  if (action.type === CLEAR_USER_EDITION) {
    return {
      ...state,
      user: null,
    };
  }

  if (action.type === DELETE_USER) {
    return {
      ...state,
      user: null,
      data: state.data.filter((user) => user.id !== action.payload),
    };
  }

  if (action.type === SAVE_USER) {
    if (action.payload.id) {
      const updateIndex = state.data.findIndex((user) => user.id === action.payload.id);
      const data = state.data;
      data[updateIndex] = action.payload;

      return {
        ...state,
        user: null,
        data,
      };
    }

    return {
      ...state,
      user: null,
      data: [
        ...state.data,
        {
          ...action.payload,
          id: state.data.length + 1,
        },
      ],
    };
  }

  return state;
};
