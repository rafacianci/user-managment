import {
  GET_GROUP,
  CLEAR_GROUP_EDITION,
  SAVE_GROUP,
  DELETE_GROUP,
} from '../actions/types';

const initialState = {
  data: [
    {
      name: 'Group 1',
      id: 1,
      users: [1, 2, 3],
    },
    {
      name: 'Group 2',
      id: 2,
      users: [],
    },
  ],
};

export default (state = initialState, action) => {
  if (action.type === GET_GROUP) {
    return {
      ...state,
      group: state.data.find((group) => group.id === parseInt(action.payload, 0)),
    };
  }

  if (action.type === CLEAR_GROUP_EDITION) {
    return {
      ...state,
      group: null,
    };
  }

  if (action.type === DELETE_GROUP) {
    return {
      ...state,
      group: null,
      data: state.data.filter((group) => group.id !== action.payload),
    };
  }

  if (action.type === SAVE_GROUP) {
    if (action.payload.id) {
      const updateIndex = state.data.findIndex((group) => group.id === action.payload.id);
      const data = state.data;
      data[updateIndex] = action.payload;

      return {
        ...state,
        group: null,
        data,
      };
    }

    return {
      ...state,
      group: null,
      data: [
        ...state.data,
        {
          ...action.payload,
          id: state.data.length + 1,
        },
      ],
    };
  }


  return state;
};
