import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { getGroup, clearEdition, save, deleteGroup } from '../../actions/group';
import './style.css';

class GroupForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      form: { },
    };

    if (props.match.params && props.match.params.groupId !== 'new') {
      props.getGroup(props.match.params.groupId);
    }
  }

  componentWillReceiveProps(props) {
    if (props.group && !this.state.form.id) {
      this.setState({
        form: props.group,
      });
    }
  }

  componentWillUnmount() {
    this.props.clearEdition();
  }

  handleChange(input, value) {
    this.setState({
      form: {
        ...this.state.form,
        [input]: value,
      },
    });
  }

  handleUsersChange(options) {
    const selectedOptions = [];
    for (let i = 0; i < options.length; i += 1) {
      if (options[i].selected) {
        selectedOptions.push(parseInt(options[i].value, 0));
      }
    }

    this.handleChange('users', selectedOptions);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.save(this.state.form);
    this.props.history.push('/groups');
  }

  handleDelete(event) {
    event.preventDefault();
    if (window.confirm('Do you realy want to delete this group?')) {
      this.props.deleteGroup(this.state.form.id);
      this.props.history.push('/groups');
    }
  }

  render() {
    return (
      <form onSubmit={(e) => this.handleSubmit(e)} className='container columns form'>
        <div className='field column is-4'>
          <label htmlFor='name' className='label'>Group Name</label>
          <div className='control'>
            <input
              name='name'
              className='input'
              type='text'
              value={this.state.form.name}
              required
              onChange={(event) => this.handleChange('name', event.target.value)}
            />
          </div>
        </div>
        <div className='field column is-4'>
          <label htmlFor='users' className='label'>Users</label>
          <div className='control'>
            <div className='select'>
              <select
                name='users'
                multiple
                value={this.state.form.users}
                onChange={(event) => this.handleUsersChange(event.target.options)}
              >
                {
                  this.props.users.map((user) => (
                    <option key={`${user.id}${user.name}`} value={user.id}>{user.name}</option>
                  ))
                }
              </select>
            </div>
          </div>
        </div>
        <div className='column is-12'>
          <Link to='/groups' className='button'>Cancel</Link>
          <button type='submit' className='button is-success is-right'>Save</button>
          {
            (this.props.match.params.groupId !== 'new') &&
            <button
              className='button is-danger is-right margin-right'
              onClick={(event) => this.handleDelete(event)}
            >
              Delete
            </button>
          }
        </div>
      </form>
    );
  }
}

const mapStateProps = ({ users, groups }) => ({
  users: users.data,
  group: groups.group,
});

export default connect(mapStateProps, {
  getGroup,
  clearEdition,
  save,
  deleteGroup,
})(GroupForm);
