import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { getUser, clearEdition, save, deleteUser } from '../../actions/user';
import './style.css';

class UserForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      form: { },
    };

    if (props.match.params && props.match.params.userId !== 'new') {
      props.getUser(props.match.params.userId);
    }
  }

  componentWillReceiveProps(props) {
    if (props.user && !this.state.form.id) {
      this.setState({
        form: props.user,
      });
    }
  }

  componentWillUnmount() {
    this.props.clearEdition();
  }

  handleChange(input, value) {
    this.setState({
      form: {
        ...this.state.form,
        [input]: value,
      },
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.save(this.state.form);
    this.props.history.push('/');
  }

  handleDelete(event) {
    event.preventDefault();
    if (window.confirm('Do you realy want to delete this user?')) {
      this.props.deleteUser(this.state.form.id);
      this.props.history.push('/');
    }
  }

  render() {
    return (
      <form onSubmit={(e) => this.handleSubmit(e)} className='container columns form'>
        <div className='field column is-4'>
          <label htmlFor='name' className='label'>User Name</label>
          <div className='control'>
            <input
              name='name'
              className='input'
              type='text'
              value={this.state.form.name}
              required
              onChange={(event) => this.handleChange('name', event.target.value)}
            />
          </div>
        </div>
        <div className='column is-12'>
          <Link to='/' className='button'>Cancel</Link>
          <button type='submit' className='button is-success is-right'>Save</button>
          {
            (this.props.match.params.userId !== 'new') &&
            <button
              className='button is-danger is-right margin-right'
              onClick={(event) => this.handleDelete(event)}
            >
              Delete
            </button>
          }
        </div>
      </form>
    );
  }
}

const mapStateProps = ({ users }) => ({
  user: users.user,
});

export default connect(mapStateProps, {
  getUser,
  clearEdition,
  save,
  deleteUser,
})(UserForm);
