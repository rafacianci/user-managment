import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

const Users = ({ users }) => (
  <div className='container'>
    <h1 className='page-title'>
      Users
      <Link to='/user/new' className='button is-success is-right'>New User</Link>
    </h1>
    <table className='table'>
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        {
          users.map((user) => (
            <Link key={`${user.id}${user.name}`} to={`/user/${user.id}`} className='edit-link'>
              <td>{user.id}</td>
              <td>{user.name}</td>
            </Link>
          ))
        }
      </tbody>
    </table>
  </div>
);

const mapStateProps = ({ users }) => ({
  users: users.data,
});

export default connect(mapStateProps, null)(Users);
