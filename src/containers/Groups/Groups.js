import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const getUserById = (data, id) => {
  const user = data.filter((obj) => obj.id === id);
  if (!user.length) {
    return null;
  }

  return (
    <li key={user[0].name}>
      { user[0].name }
    </li>
  );
};

const getUsers = (users, groupUsers) => {
  if (!groupUsers.length) {
    return 'There is no users for this group';
  }

  return (
    <ul>
      {
        groupUsers.map((userId) => getUserById(users, userId))
      }
    </ul>
  );
};

const Groups = ({ groups, users }) => (
  <div className='container'>
    <h1 className='page-title'>
      Groups
      <Link to='/group/new' className='button is-success is-right'>New Group</Link>
    </h1>
    <table className='table'>
      <thead>
        <tr>
          <th>#</th>
          <th>Group</th>
          <th>Users</th>
        </tr>
      </thead>
      <tbody>
        {
          groups.map((group) => (
            <Link key={`${group.id}${group.name}`} to={`/group/${group.id}`} className='edit-link'>
              <td>{group.id}</td>
              <td>{group.name}</td>
              <td>
                {
                  getUsers(users, group.users)
                }
              </td>
            </Link>
          ))
        }
      </tbody>
    </table>
  </div>
);

const mapStateProps = ({ groups, users }) => ({
  groups: groups.data,
  users: users.data,
});

export default connect(mapStateProps, null)(Groups);
