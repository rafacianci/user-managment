export const GET_USER = 'get_user';
export const CLEAR_USER_EDITION = 'clear_user_edition';
export const SAVE_USER = 'save_user';
export const DELETE_USER = 'DELETE_user';

export const GET_GROUP = 'get_group';
export const CLEAR_GROUP_EDITION = 'clear_group_edition';
export const SAVE_GROUP = 'save_group';
export const DELETE_GROUP = 'delete_group';
