import {
  GET_USER,
  CLEAR_USER_EDITION,
  SAVE_USER,
  DELETE_USER,
} from './types';

export const getUser = (id) => ({
  type: GET_USER,
  payload: id,
});

export const clearEdition = () => ({
  type: CLEAR_USER_EDITION,
});

export const save = (user) => ({
  type: SAVE_USER,
  payload: user,
});

export const deleteUser = (id) => ({
  type: DELETE_USER,
  payload: id,
});
