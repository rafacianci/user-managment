import {
  GET_GROUP,
  CLEAR_GROUP_EDITION,
  SAVE_GROUP,
  DELETE_GROUP,
} from './types';

export const getGroup = (id) => ({
  type: GET_GROUP,
  payload: id,
});

export const clearEdition = () => ({
  type: CLEAR_GROUP_EDITION,
});

export const save = (group) => ({
  type: SAVE_GROUP,
  payload: group,
});

export const deleteGroup = (id) => ({
  type: DELETE_GROUP,
  payload: id,
});
